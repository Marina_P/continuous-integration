// initialize
console.log('program starting');
const PORT = 3000;
const HOST = "127.0.0.1" // all 0.0.0.0
const router = require('./router');
// operate
router.listen(
    PORT,
    HOST, () => console.log(`Listening to http://${HOST}:${PORT}`)
); //backticks
// cleanup
console.log('program ending')